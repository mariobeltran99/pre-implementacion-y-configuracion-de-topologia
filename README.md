# Pre-Implementación y Configuración de Topología

Pre-Implementación y Configuración de Topología con equipos CISCO y simulación con CISCO Packet Tracer 6.2

* Gestión de contraseñas enable en los routers y switch's.
* Utilización del protocolo de enrutamiento RIPv2
* Topología de estrella extendida
* Configuración de una red LAN para una pequeña empresa.
